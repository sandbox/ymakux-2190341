(function ($) {
  Drupal.behaviors.shipping = {
    attach: function (context, settings) {

      var lat;
      var lng;
      var map;
      var locationSelect;
      var codedAddress;
      var loading = "<div class='loading'>" + Drupal.t("Loading...") + "</div>";
      
      if ($("#search-warehouses-wrapper").length) {
        initializeWarehouseMap();

        $('a.popular-search').click(function(){
          var address = $(this).text();
          $('input#addressInput').val(address);
          searchWarehouses(address);
          return false;
        });
      }

      function searchWarehouses(address) {
        getCoordinates(address, function(codedAddress) {
          if (codedAddress) {
            lat = codedAddress.lat();
            lng = codedAddress.lng();
            searchNearWarehouses(lat, lng, address);
          }
        });
      }

      $('#warehouses-search-results .warehouse').live('click', function(){
        $('#warehouses-search-results .warehouse').removeClass('active');
        $('#warehouses-search-results .warehouse .details').hide();
        $(this).children('.details').show();
        $(this).addClass('active');
        var lat = $(this).attr('data-lat');
        var lng = $(this).attr('data-lng');
        if($.cookie != 'undefined') {
          $.cookie('shippingWarehouseId', $(this).attr('data-warehouse-id'), {path: '/'});
        }
        initializeGoogleMapMarker(lat, lng);
      });
    
      function searchNearWarehouses(lat, lng, address) {
        var latlng;
        var searchUrl = settings.basePath + 'shipping/markers?lat=' + lat + '&lng=' + lng;
        
        $.get(searchUrl, function(data) {
          if('markers' in data) {
            $('#search-warehouses-wrapper .results-found').text(Drupal.t('Found @num warehouse(s)', {'@num': data.markers.length}));
            $('#warehouses-search-results').html(data.html);
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0; i < data.markers.length; i++) {
              latlng = new google.maps.LatLng(parseFloat(data.markers[i].lat), parseFloat(data.markers[i].lng));
              createMarker(latlng, data.markers[i]);
              bounds.extend(latlng);
            }
            map.fitBounds(bounds);
           
           if($.cookie != 'undefined') {
              $.cookie('shippingSuggestedAddress', rawurlencode(address), {path: '/'});
              $.cookie('shippingSuggesteddLatLng', lat + '|' + lng, {path: '/'});
            }
          }
          else {
            alert(Drupal.t('Your search yielded no results'));
          }
        });
      }

      function createMarker(latlng, warehouse) {
        var markerOptions = {map: map, position: latlng, id: warehouse.id};
        if('icon' in warehouse) {
          markerOptions.icon = warehouse.icon;
        }

        var marker = new google.maps.Marker(markerOptions);
        google.maps.event.addListener(marker, 'click', function() {
          
        if($.cookie != 'undefined') {
          $.cookie('shippingWarehouseId', marker.id, {path: '/'});
        }

        var item = $('#warehouses-search-results #warehouse-' + marker.id);
        $('#warehouses-search-results .warehouse').removeClass('active');
        $('#warehouses-search-results .warehouse .details').hide();
        item.addClass('active').children('.details').show();

        var results = $('#warehouses-search-results');
          results.scrollTop(results.scrollTop() + (item.position().top - results.position().top) - (results.height()/2) + (item.height()/2));
        });
        return marker;
      }
      
      function getCoordinates(address, callback) {
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({"address": address}, function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            codedAddress = results[0].geometry.location;
          }
          callback(codedAddress);
        });
      }

      function initializeGoogleMap(lat, lng) {
        map = new google.maps.Map(document.getElementById("google-map"), {
          center: new google.maps.LatLng(lat, lng),
          zoom: 14,
          mapTypeId: 'roadmap',
          mapTypeControl: false,
        });
      }
      
    var address;
    var selectors = "input#warehouse-address,input#warehouse-city,input#warehouse-state,select#warehouse-country";
    $(selectors).bind("change keyup", function(event) {
      address = $(selectors).map(function() {return this.value;}).get().filter(function(e){return e}).join(",");
      if ($("div#google-map-wrapper").is(":visible")){
        initializeGoogleMapBranch(address);
      }
    }).keyup();

    $("a#load-map", context).toggle(function() {
      $("div#google-map-wrapper").show();
      $(this).text(Drupal.t("Hide map"));
      initializeGoogleMapBranch(address);
      return false;
    },
    function() {
      $(this).text(Drupal.t("Show on map"));
      $("div#google-map-wrapper").hide();
      return false;
    });

    var quickLook = $('a.warehouse-quick-look');

    quickLook.show();
    quickLook.click(function() {
      var url = this.href;
      var title = $(this).attr("title");
      var params = url.split("?")[1].split("&");
      lat = params[0].split("=")[1];
      lng = params[1].split("=")[1];
      var dialog = $(loading).appendTo("body");
      dialog.dialog({
        close: function(event, ui) {
          dialog.remove();
        },
        modal: true,
        height: 500,
        width: 700,
        title: title,
      });
        
      dialog.load(
        url, 
        {},
        function (responseText, textStatus, XMLHttpRequest) {
          dialog.removeClass("loading");
          initializeGoogleMapMarker(lat, lng);
        }
      );
        return false;
    }); 
    
    var searhWarehouse = $('a.search-warehouse-popup');
    searhWarehouse.show();
    if('shipping' in settings && 'useColorbox' in settings.shipping) {
      searhWarehouse.colorbox({
        href: function(){
          return $(this).attr('href') + ' #search-warehouses-wrapper';
        },
        width: '80%',
        height: 600,
        onComplete:function() {
          initializeWarehouseMap();
        },
      });
      return false;
    }

    searhWarehouse.click(function() {
      var dialog = $(loading).appendTo("body");
      dialog.dialog({
        close: function(event, ui) {
          dialog.remove();
        },
        modal: true,
        height: 580,
        width: 960,
      });
        
      dialog.load(
        settings.basePath + 'search-warehouses #search-warehouses-wrapper', 
        {},
        function (responseText, textStatus, XMLHttpRequest) {
          dialog.removeClass("loading");
          initializeWarehouseMap();
        }
      );
        return false;
    });
    
    
    function initializeGoogleMapBranch(address) {
      if (typeof google === "object" && typeof google.maps === "object") {
        getCoordinates(address, function(codedAddress) {
          if (codedAddress) {
            
            lat = codedAddress.lat();
            lng = codedAddress.lng();            
            
            $("div#google-map").html(loading);
            initializeGoogleMapMarker(lat, lng);

            $("input#warehouse-lat").val(lat);
            $("input#warehouse-lng").val(lng);
          }
        });
      }
    }
    
    function initializeGoogleMapMarker(lat, lng) {
      var coordinates = new google.maps.LatLng(lat, lng);
      map = new google.maps.Map(document.getElementById("google-map"), {
        center: coordinates,
        zoom: 14,
        mapTypeId: 'roadmap',
        mapTypeControl: false,
      });

      var marker = new google.maps.Marker({position: coordinates});
      marker.setMap(map);
    }
      
    function rawurlencode(str){
      str = (str + '').toString();
      return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
        replace(/\)/g, '%29').replace(/\*/g, '%2A');
    }

    function initializeWarehouseMap() {
      $('#search-warehouses-wrapper').show();
      var wrapper = $('#search-warehouses-wrapper');
      wrapper.ajaxStart(function(){
        $('input#searchLocations').attr('disabled', 'disabled');
        wrapper.addClass('loading');
      }).ajaxSuccess(function(){
        wrapper.removeClass('loading');
        $('input#searchLocations').removeAttr('disabled');
      });
      
      initializeGoogleMap(settings.shipping.warehouseSearchLat, settings.shipping.warehouseSearchLng);
        var autocompleteInput = document.getElementById('addressInput');
        var autocomplete = new google.maps.places.Autocomplete(autocompleteInput);
        $("#searchLocations", context).live('click', function() {
          var address = autocompleteInput.value;
          if(address != "") {
            searchWarehouses(address);
          }
        });  
      }
    }
  };
})(jQuery);
