<?php

function theme_shipping_warehouse_search($variables) {

  _shipping_add_files();

  $output = '<noscript>' . t('For full functionality of this site it is necessary to <a href="http://www.enable-javascript.com/" target="_blank" rel="nofollow">enable</a> JavaScript.') . '</noscript>';
  $output .= '<div id="search-warehouses-wrapper" style="display:none;">';
  $output .= '<table id="google-map-content">
        <tr>
          <td class="sidebar">
            <div id="filter">
              <div class="address">
                <input type="text" id="addressInput" class="form-text" size="10"/>
                ' . theme('shipping_popular_searches') . '
              </div>
              <div class="search">
                <input type="button" class="form-submit" id="searchLocations" name="search" value="' . t('Search within @distkm', array('@dist' => variable_get('shipping_warehouse_search_radius', 50))) . '"/>
              </div>
              <div class="results-found"></div>
            </div>
            <div id="warehouses-search-results"></div>
           </td>
         <td class="map">
         <div class="google-map-container">
           <div id="google-map"></div>
           <div class="loading-overlay"></div>
           </div>
         </td>
       </tr>
     </table>
    
   </div>';  
  return $output;
}

function theme_shipping_warehouse_search_popup($variables) {
  if(user_access('shipping use google map')) {
    _shipping_add_files();

   if(module_exists('libraries') && ($library = libraries_detect('colorbox')) && !empty($library['installed'])) {
     libraries_load('colorbox');
     $use_colorbox = TRUE;
     drupal_add_js(array('shipping' => array('useColorbox' => TRUE)), 'setting');
   }
   
   if(empty($use_colorbox)) {
     drupal_add_library('system', 'ui.dialog');
   }

   return l(t('Search warehouses'), 'search-warehouses', array(
      'attributes' => array(
        'class' => array('search-warehouse'),
        'style' => 'display:none;',
      ),
    ));
  }
  return '';
}

function theme_shipping_search_results($variables) {
  $items = '';
  foreach($variables['warehouses'] as $warehouse) {
    $warehouse['data'] = unserialize($warehouse['data']);
    $items .= theme('shipping_search_result', array('warehouse' => $warehouse));
  }
  return $items;
}

function theme_shipping_popular_searches($variables) {
  $items = array();
  foreach(variable_get('shipping_warehouse_popular_searches', array()) as $title) {
    $items[] = '<a href="#" class="popular-search">' . check_plain(trim($title)) . '</a>';
  }
  return '<div class="popular-searches">' . theme('item_list', array('items' => $items)) . '</div>';
}

function theme_shipping_search_result($variables) {
  $output = '';
  $warehouse = $variables['warehouse'];
  if($service = shipping_service_load($warehouse['service'])) {
    $service_title = !empty($service['display_title']) ? check_plain($service['display_title']) : $service['title'];
    $output = '<div id="warehouse-' . $warehouse['id'] . '" 
    class="warehouse" data-lng="' . $warehouse['lng'] . '" 
    data-lat="' . $warehouse['lat'] . '" 
    data-warehouse-id="' . $warehouse['id'] . '">';
    $output .= '<div class="service">' . $service_title . '</div>';
    $output .= '<div class="title">' . check_plain($warehouse['title']) . '</div>';

    $output .= '<div class="details" style="display:none;">';
    $output .= '<div class="address">' . check_plain($warehouse['address']) . '</div>';
    if(!empty($warehouse['data']['phone'])) {
      $output .= '<div class="phone">' . check_plain($warehouse['data']['phone']) . '</div>';
    }
    $output .= '</div>';
    $output .= '</div>';
  }
  return $output;
}

function theme_shipping_warehouse_info_table($variables) {

  $data = isset($variables['warehouse']['data']) ? $variables['warehouse']['data'] : array();

  $info = array(
    array(array('data' => t('Phone'),'header' => TRUE), isset($data['phone']) ? check_plain($data['phone']) : ''),
    array(array('data' => t('Max weight'),'header' => TRUE), !empty($data['weight']) ? check_plain($data['weight']) : t('Unlimited')),
    array(array('data' => t('Business days'),'header' => TRUE, 'colspan' => 2)),
    
    /*
    array(array('data' => t('Collect hours'),'header' => TRUE), isset($data['time']['week']['receive']) ? check_plain($data['time']['week']['receive']) : ''),
    array(array('data' => t('Dispatch hours'),'header' => TRUE), isset($data['time']['week']['send']) ? check_plain($data['time']['week']['send']) : ''),
    array(array('data' => t('Opening hours'),'header' => TRUE), isset($data['time']['week']['work']) ? check_plain($data['time']['week']['work']) : ''),
    array(array('data' => t('Saturday'),'header' => TRUE, 'colspan' => 2)),
    array(array('data' => t('Collect hours'),'header' => TRUE), isset($data['time']['saturday']['receive']) ? check_plain($data['time']['saturday']['receive']) : ''),
    array(array('data' => t('Dispatch hours'),'header' => TRUE), isset($data['time']['saturday']['send']) ? check_plain($data['time']['saturday']['send']) : ''),
    array(array('data' => t('Opening hours'),'header' => TRUE), isset($data['time']['saturday']['work']) ? check_plain($data['time']['saturday']['work']) : ''),
  
  */

  );
  return theme('table', array('rows' => $info, 'attributes' => array('class' =>array('warehouse-info'))));
}

function theme_shipping_warehouse_info($variables) {
  $output = '';
  if ($warehouse = (array) shipping_warehouse_load($variables['warehouse'])) {
    $info = theme('shipping_warehouse_info_table', array('warehouse' => $warehouse));
    $map = '<div id="google-map" style="height:400px; width:400px;"></div>';
    $output .= theme('table', array('rows' => array(array($map, $info))));
  }
  return $output;
}

function theme_shipping_warehouses($variables) {
  $header = array(
    array('data' => t('ID'), 'field' => 'id', 'sort' => 'asc'),
    array('data' => t('Address'), 'field' => 'address'),
    array('data' => t('City'), 'field' => 'city'),
    array('data' => t('State'), 'field' => 'state'),
    array('data' => t('Country'), 'field' => 'country'),
    array('data' => t('Service'), 'field' => 'service'),
    array('data' => t('Module'), 'field' => 'module'),
    array('data' => t('Operations')),
  );

  $query = db_select('shipping_warehouses', 'w')
    ->fields('w')
    ->extend('TableSort')
    ->extend('PagerDefault')
    ->limit(20);

  $warehouses = $query->orderByHeader($header)->execute();

  $rows = array();
  if ($warehouses) {
    foreach ($warehouses as $warehouse) {

      $links = array();
      $links[] = array(
        'title' => t('Edit'),
        'href' => 'admin/structure/shipping/warehouses/' . $warehouse->id . '/edit',
        'query' => array('destination' => $_GET['q']),
      );
      
      $links[] = array(
        'title' => t('Quick look'),
        'href' => 'shipping/quick-look/' . $warehouse->id,
        'attributes' => array(
          'title' => t('@service: @address, @city, @country', array(
            '@service' => $warehouse->service,
            '@address' => $warehouse->address,
            '@city' => $warehouse->city,
            '@country' => $warehouse->country,
          )),
          'class' => array('warehouse-quick-look')),
        'query' => array('lat' => $warehouse->lat, 'lng' => $warehouse->lng),
      );
      
      $links[] = array(
        'title' => t('Delete'),
        'href' => 'admin/structure/shipping/warehouses/' . $warehouse->id . '/delete',
        'query' => array('destination' => $_GET['q']),
      );
      
      $rows[] = array(
        array('data' => check_plain($warehouse->id)),
        array('data' => check_plain($warehouse->address)),
        array('data' => check_plain($warehouse->city)),
        array('data' => check_plain($warehouse->state)),
        array('data' => check_plain($warehouse->country)),
        array('data' => check_plain($warehouse->service)),
        array('data' => check_plain($warehouse->module)),
        array('data' => theme('links', array('links' => $links,'attributes' => array('class' => array('links', 'inline'))))),
      );
    }
  }
  
  $library = 'http://maps.googleapis.com/maps/api/js?sensor=false';
  drupal_add_js($library, array('type' => 'external', 'weight' => 0));
  drupal_add_js(drupal_get_path('module', 'shipping') . '/shipping.js');
  drupal_add_css(drupal_get_path('module', 'shipping') . '/shipping.css');
  drupal_add_library('system', 'ui.dialog');

  $empty = t('No warehouses in your database');
  return theme('table', array('rows' => $rows, 'header' => $header, 'empty' => $empty)) . theme('pager');
}

function theme_shipping_services($variables) {
  $header = array(
    array('data' => t('ID'), 'field' => 'id', 'sort' => 'asc'),
    array('data' => t('Title'), 'field' => 'title'),
    array('data' => t('Display title'), 'field' => 'display_title'),
    array('data' => t('Active'), 'field' => 'active'),
    array('data' => t('Operations')),
  );

  $query = db_select('shipping_services', 's')
    ->fields('s')
    ->extend('TableSort')
    ->extend('PagerDefault')
    ->limit(10);

  $services = $query->orderByHeader($header)->execute();

  $rows = array();
  if ($services) {
    foreach ($services as $service) {

      $links = array();
      $links[] = array(
        'title' => t('Edit'),
        'href' => 'admin/structure/shipping/services/' . $service->id . '/edit',
        'query' => array('destination' => $_GET['q']),
      );
      
      $links[] = array(
        'title' => t('Delete'),
        'href' => 'admin/structure/shipping/services/' . $service->id . '/delete',
        'query' => array('destination' => $_GET['q']),
      );
      
      $rows[] = array(
        array('data' => check_plain($service->id)),
        array('data' => check_plain($service->title)),
        array('data' => check_plain($service->display_title)),
        array('data' => check_plain($service->active)),
        array('data' => theme('links', array('links' => $links,'attributes' => array('class' => array('links', 'inline'))))),
      );
    }
  }

  $empty = t('No services in your database');
  return theme('table', array('rows' => $rows, 'header' => $header, 'empty' => $empty)) . theme('pager');
}
