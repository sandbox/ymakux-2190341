<?php

define('SHIPPING_GEOCODE_URL', 'http://maps.googleapis.com/maps/api/geocode/json');

/**
 * Implements hook_permission().
*/
function shipping_permission() {
  return array(
    'shipping use google map' => array(
      'title' => t('Use Google Maps to search and display shipping services'),
    ),
    'shipping administer' => array(
      'title' => t('Add, edit, delete shipping services'),
    ),
    'shipping administer warehouses' => array(
      'title' => t('Add, edit, delete warehouses of any shipping service'),
    ),
    'shipping administer services' => array(
      'title' => t('Add, edit, delete any shipping service'),
    ),
  );
}

/**
 * Implements hook_menu().
*/
function shipping_menu() {
  
  $items = array();
  $items['admin/structure/shipping'] = array(
    'title'            => 'Shipping',
    'page callback'    => 'shipping_warehouses',
    'access arguments' => array('shipping administer warehouses'),
    'file'             => 'shipping.pages.inc',
  );
  
  $items['admin/structure/shipping/services'] = array(
    'title'            => 'Services',
    'page callback'    => 'shipping_services',
    'access arguments' => array('shipping administer services'),
    'file'             => 'shipping.pages.inc',
    'type'             => MENU_LOCAL_TASK,
    'weight'           => 0,
  );
  
  $items['admin/structure/shipping/services/add'] = array(
    'title'            => 'Add service',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('shipping_service_edit_form'),
    'access arguments' => array('shipping administer services'),
    'file'             => 'shipping.admin.inc',
    'type'             => MENU_LOCAL_ACTION,
  );
  
  $items['admin/structure/shipping/services/%shipping_service/edit'] = array(
    'title'            => 'Edit',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('shipping_service_edit_form', 4),
    'access arguments' => array('shipping administer services'),
    'file'             => 'shipping.admin.inc',
  );
  
  $items['admin/structure/shipping/services/%shipping_service/delete'] = array(
    'title'            => 'Delete',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('shipping_service_delete_form', 4),
    'access arguments' => array('shipping administer services'),
    'file'             => 'shipping.admin.inc',
  ); 

  $items['admin/structure/shipping/warehouses'] = array(
    'title'            => 'Warehouses',
    'type'             => MENU_DEFAULT_LOCAL_TASK,
    'weight'           => 10,
  );
    
  $items['admin/structure/shipping/settings'] = array(
    'title'            => 'Settings',
    'page callback'    => 'drupal_get_form',
    'page arguments'    => array('shipping_settings_form'),
    'access arguments' => array('shipping administer'),
    'file'             => 'shipping.admin.inc',
    'type'             => MENU_LOCAL_TASK,
    'weight'           => 20,
  );


  $items['admin/structure/shipping/map'] = array(
    'title'            => 'Map',
    'page callback'    => 'shipping_warehouse_search',
    'access arguments' => array('shipping use google map'),
    'file'             => 'shipping.pages.inc',
    'type'             => MENU_LOCAL_TASK,
    'weight'           => 30,
  );

  $items['admin/structure/shipping/warehouses/add'] = array(
    'title'            => 'Add warehouse',
    'description'      => 'Add new warehouse',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('shipping_warehouse_edit_form'),
    'access arguments' => array('shipping administer warehouses'),
    'file'             => 'shipping.admin.inc',
    'type'             => MENU_LOCAL_ACTION,
  );
  
  $items['admin/structure/shipping/warehouses/upload'] = array(
    'title'            => 'Upload from CSV',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('shipping_warehouse_upload_form'),
    'access arguments' => array('shipping administer warehouses'),
    'file'             => 'shipping.admin.inc',
    'type'             => MENU_LOCAL_ACTION,
  );

  $items['admin/structure/shipping/warehouses/%shipping_warehouse/delete'] = array(
    'title'            => 'Delete warehouse',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('shipping_warehouse_delete_form', 4),
    'access arguments' => array('shipping administer warehouses'),
    'file'             => 'shipping.admin.inc',
    'type'             => MENU_CALLBACK,
  );
  
  $items['admin/structure/shipping/warehouses/%shipping_warehouse/edit'] = array(
    'title'            => 'Edit warehouse',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('shipping_warehouse_edit_form', 4),
    'access arguments' => array('shipping administer warehouses'),
    'file'             => 'shipping.admin.inc',
    'type'             => MENU_CALLBACK,
  );
  
  $items['shipping'] = array(
    'page callback'   => 'shipping_page',
    'access callback' => TRUE,
    'type'            => MENU_CALLBACK,
    'file'            => 'shipping.pages.inc',
  );
  
  $items['search-warehouses'] = array(
    'title'            => 'Search warehouses',
    'page callback'    => 'shipping_warehouse_search',
    'access arguments' => array('shipping use google map'),
    'file'             => 'shipping.pages.inc',
    'type' => MENU_SUGGESTED_ITEM,
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function shipping_theme() {
  return array(
    'shipping_services' => array(
      'variables' => array(),
      'file' => 'shipping.theme.inc',
    ),
    'shipping_warehouses' => array(
      'variables' => array(),
      'file' => 'shipping.theme.inc',
    ),
    'shipping_gmap_address' => array(
      'variables' => array(),
      'file' => 'shipping.pages.inc',
    ),
    'shipping_search_results' => array(
      'variables' => array('warehouses' => array()),
      'file' => 'shipping.theme.inc',
    ),
    'shipping_search_result' => array(
      'variables' => array('warehouse' => array()),
      'file' => 'shipping.theme.inc',
    ),
    'shipping_warehouse_search_popup' => array(
      'variables' => array(),
      'file' => 'shipping.theme.inc',
    ),
    'shipping_popular_searches' => array(
      'variables' => array(),
      'file' => 'shipping.theme.inc',
    ),
    'shipping_warehouse_search' => array(
      'variables' => array(),
      'file' => 'shipping.theme.inc',
    ),
    'shipping_warehouse_info' => array(
      'variables' => array('warehouse' => NULL),
      'file' => 'shipping.theme.inc',
    ),
    'shipping_warehouse_info_table' => array(
      'variables' => array('warehouse' => NULL),
      'file' => 'shipping.theme.inc',
    ),
  );
}

function shipping_get_nearby($lat, $lng) {
  $output = array();
  $radius = variable_get('shipping_warehouse_search_radius', 50);
  $query = "SELECT *, (6371 * acos(cos(radians(:lat)) * cos(radians(lat)) * cos(radians(lng) - radians(:lng)) + sin(radians(:lat)) * sin(radians(lat)))) AS distance FROM {shipping_warehouses} HAVING distance < :dist ORDER BY distance LIMIT 0, 20";
  $result = db_query($query, array(':lat' => $lat, ':lng' => $lng, ':dist' => $radius))->fetchAllAssoc('id');
  
  if ($result) {
    foreach ($result as $warehouse) {
      $output[$warehouse->service][$warehouse->id] = (array) $warehouse;
    }
  }
  return $output;
}

function shipping_geocode_address($address) {
  $query = drupal_http_request(url(SHIPPING_GEOCODE_URL, array('query' => array('address' => $address, 'sensor' => 'false'))));
  if(!empty($query->data) && $query->code == 200) {
    $response = json_decode($query->data, TRUE);
    if($response['status'] == 'OK' && !empty($response['results'][0]['geometry']['location'])) {
      return $response['results'][0]['geometry']['location'];
    }
  }
}

function shipping_warehouse_load($id) {
  $warehouse = db_select('shipping_warehouses', 'w')
    ->fields('w')
    ->condition('w.id', $id)
    ->execute()
    ->fetchObject();
  
  if($warehouse) {
    $warehouse->data = unserialize($warehouse->data);
    return (array) $warehouse;
  }
  return FALSE;
}

function shipping_service_load($id) {
  $service = db_select('shipping_services', 's')
    ->fields('s')
    ->condition('s.id', $id)
    ->execute()
    ->fetchObject();
  
  if($service) {
    $service->data = unserialize($service->data);
    return (array) $service;
  }
  return FALSE;
}

function shipping_warehouse_default() {
  return array(
    'id' => NULL,
    'service' => '',
    'title' => '',
    'description' => '',
    'country' => '',
    'city' => '',
    'state' => '',
    'address' => '',
    'lat' => '',
    'lng' => '',
    'module' => '',
    'data' => array(),
  );
}

function shipping_service_default() {
  return array(
    'id' => NULL,
    'title' => '',
    'display_title' => '',
    'active' => 1,
    'logo' => '',
    'description' => '',
    'data' => array(),
  );
}

function shipping_warehouse_set($warehouse) {
  
  $warehouse += shipping_warehouse_default();
  
  if (is_array($warehouse['data'])) {
    $warehouse['data'] = serialize($warehouse['data']);
  }

  return db_merge('shipping_warehouses')
    ->key(array('id' => $warehouse['id']))
    ->fields($warehouse)
    ->execute();
}

function shipping_service_set($service) {
  $service += shipping_service_default();
  if (is_array($service['data'])) {
    $service['data'] = serialize($service['data']);
  }
  return db_merge('shipping_services')
    ->key(array('id' => $service['id']))
    ->fields($service)
    ->execute();
}

function shipping_service_delete($id) {
  return db_delete('shipping_services')->condition('id', $id)->execute();
}

function shipping_services_options() {
  $services = db_select('shipping_services', 's')
    ->fields('s')
    ->execute()
    ->fetchAll();
    
  $options = array();
  if($services) {
    foreach($services as $service) {
      $options[$service->id . '|shipping'] = check_plain($service->title);
    }
  }

  if(function_exists('dcart_shipping_service_options')) {
    foreach(dcart_shipping_service_options(NULL, TRUE) as $name => $title) {
      $options[$name . '|dcart'] = $title . ' (Dcart shipping service)';
    }
  }
  return $options;
}

function shipping_warehouse_delete($id) {
  return db_delete('shipping_warehouses')->condition('id', $id)->execute();
}

function shipping_dcart_product_info_type() {
  return array(
    'search_warehouse' => array(
      'title' => NULL,
      'callback' => 'shipping_product_info_search_warehouse',
    ),
  );
}

function shipping_product_info_search_warehouse_form($form, $form_state) {

  return array(
    'search' => array(
      '#type' => 'link',
      '#title' => t('Search warehouses'),
      '#href' => 'search-warehouses',
      '#attributes' => array(
        'target' => '_blank',
        'rel' => 'nofollow',
        'class' => array('search-warehouse'),
        'fragment' => 'search-warehouses-wrapper',
      ),
      '#access' => user_access('shipping use google map'),
     ),
   );
}

function _shipping_add_files() {
  $library = "http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places";
  drupal_add_js($library, array('type' => 'external', 'weight' => 0)); 
  drupal_add_js(drupal_get_path('module', 'shipping') . '/shipping.js');
  drupal_add_css(drupal_get_path('module', 'shipping') . '/shipping.css');
  drupal_add_library('system', 'jquery.cookie');
  
  $settings = array(
    'mapRadius' => variable_get('shipping_warehouse_radius', 10),
    'warehouseSearchLat' => variable_get('shipping_warehouse_search_lat', 50.4501),
    'warehouseSearchLng' => variable_get('shipping_warehouse_search_lng', 30.523400000000038),
    'warehouseSearchRadius' => variable_get('shipping_warehouse_search_radius', 50),
  );
  drupal_add_js(array('shipping' => $settings), 'setting');
}
