<?php

function shipping_service_edit_form($form, &$form_state, $service = NULL) {
  $form['#tree'] = TRUE;
  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => isset($service['title']) ? $service['title'] : '',
    '#maxlength' => 255,
    '#description' => t('Human name of the service'),
  );
  $form['id'] = array(
    '#title' => t('Id'),
    '#type' => 'machine_name',
    '#default_value' => isset($service['id']) ? $service['id'] : NULL,
    '#maxlength' => 32,
    '#required' => TRUE,
    '#disabled' => isset($service['id']),
    '#machine_name' => array(
      'exists' => 'shipping_service_load',
      'source' => array('title'),
    ),
  );
  $form['display_title'] = array(
    '#title' => t('Display title'),
    '#type' => 'textfield',
    '#default_value' => isset($service['display_title']) ? $service['display_title'] : '',
    '#maxlength' => 255,
    '#description' => t('Title to be shown for user'),
  );
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textfield',
    '#default_value' => isset($service['description']) ? $service['description'] : '',
    '#maxlength' => 255,
    '#description' => t('Short description for administrator.'),
  );
  $form['active'] = array(
    '#title' => t('Active'),
    '#type' => 'checkbox',
    '#default_value' => isset($service['active']) ? $service['active'] : 1,
    '#description' => t('State of this shipping service. Inactive services will be hidden for users'),
  );
  $form['logo'] = array (
    '#type' => 'file',
    '#title' => t('Logo'),
    '#description' => t('Logo of the service'),
  );
  if (!empty($service['logo'])) {
    if ($file = file_load($service['logo'])) {
      $form_state['service_logo_file'] = $file;
      $form['logo']['#suffix'] = theme('image_style', array(
        'style_name' => 'thumbnail',
        'path' => $file->uri));

      $form['delete_logo'] = array(
        '#title' => t('Delete logo'),
        '#type' => 'checkbox',
        '#default_value' => 0,
      );
    }
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function shipping_service_edit_form_validate($form, &$form_state) {
  form_state_values_clean($form_state);
  foreach ($form_state['values'] as $key => $value) {
    if (is_string($value)) {
      $form_state['values'][$key] = trim($value);
    }
  }

  if (!empty($form_state['service_logo_file'])) {
    if (!empty($form_state['values']['delete_logo'])) {
      file_delete($form_state['service_logo_file']);
    }
    else {
      $form_state['values']['logo'] = $form_state['service_logo_file']->fid;
    }
  }
  
  unset($form_state['values']['delete_logo']);
  
  if (!empty($_FILES['files']['name']['logo'])) {
    
    $dir = 'public://shipping/logos';
    if (file_prepare_directory($dir, FILE_CREATE_DIRECTORY)) {
      $file = file_save_upload('logo');
      $file->status = 1;
      $file = file_save($file);
      $file = file_move($file, $dir);
      $form_state['values']['logo'] = $file->fid;
    }
  }
  elseif(!$form_state['values']['logo']) {
    $form_state['values']['logo'] = 0;
  }
}

function shipping_service_edit_form_submit($form, &$form_state) {
  shipping_service_set($form_state['values']);
  drupal_set_message(t('The configuration options have been saved.'));
  $form_state['redirect'] = 'admin/structure/shipping/services';
}

function shipping_settings_form($form, $form_state) {
  $form['actions']['truncate'] = array(
    '#type' => 'submit',
    '#value' => t('Truncate all warehouses'),
    '#limit_validation_errors' => array(),
    '#submit' => array('shipping_warehouse_truncate_submit'),
    '#weight' => 20,
  );
  if(!empty($form_state['values']['truncate'])) {
    $form['actions']['truncate']['#prefix'] = t('Please confirm') . '<br />';
    $form_state['truncate_warehouses'] = TRUE;
    return $form;
  }
  $form['shipping_upload_geocode'] = array(
    '#title' => t('Geocode address automatically'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('shipping_upload_geocode', 0),
    '#description' => t('If latitude and longitude aren\'t provided, try to get them via Google Geocode API during import from CSV file.'),
  );
  $form['shipping_warehouse_search_lat'] = array(
    '#title' => t('Default latitude'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('shipping_warehouse_search_lat', 50.4501),
  );
  $form['shipping_warehouse_search_lng'] = array(
    '#title' => t('Default longitude'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('shipping_warehouse_search_lng', 30.523400000000038),
  );
  $form['shipping_warehouse_search_radius'] = array(
    '#title' => t('Search radius'),
    '#type' => 'textfield',
    '#size' => 2,
    '#required' => TRUE,
    '#default_value' => variable_get('shipping_warehouse_search_radius', 50),
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $popular_searches = variable_get('shipping_warehouse_popular_searches', array());
  $form['shipping_warehouse_popular_searches'] = array(
    '#title' => t('Predefined searches'),
    '#type' => 'textarea',
    '#rows' => 3,
    '#default_value' => implode("\n", $popular_searches),
    '#description' => t('City names to be shown in "Popular searches".'),
  );
  $form['#validate'] = array('shipping_settings_form_validate');
  return system_settings_form($form);
}

function shipping_settings_form_validate($form, &$form_state) {
  $popular_searches = trim($form_state['values']['shipping_warehouse_popular_searches']);
  if($popular_searches) {
    $form_state['values']['shipping_warehouse_popular_searches'] = explode("\n", $popular_searches);
  }
}

function shipping_warehouse_truncate_submit($form, &$form_state) {
  if(empty($form_state['truncate_warehouses'])) {
    $form_state['rebuild'] = TRUE;
  }
  else {
    db_delete('shipping_warehouses')->execute();
    drupal_set_message(t('All warehouses have been deleted from database.'));
    $form_state['redirect'] = 'admin/structure/shipping/warehouses';
  }
}

function shipping_warehouse_upload_form($form, $form_state) {
  $form['file'] = array (
    '#type' => 'file',
    '#title' => 'Choose file',
    '#description' => 'Upload cities from CSV file.',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Upload'),
  );
  return $form;
}

function shipping_warehouse_upload_form_validate($form, &$form_state) {
  if (empty($_FILES['files']['name']['file'])) {
    form_set_error('', t('Please choose file'));
    return;
  }
  $validators = array('file_validate_extensions' => array('csv'));
  if ($file = file_save_upload('file', $validators)) {
    $form_state['values']['file'] = drupal_realpath($file->uri);
  }
}

function shipping_warehouse_upload_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['file'])) {
    include_once 'shipping.pages.inc';
    shipping_batch_csv_set($form_state['values']['file']);
    batch_process('admin/structure/shipping');
  }
}

function shipping_warehouse_edit_form($form, &$form_state, $warehouse = NULL) {

  $shipping_services = shipping_services_options();
  
  if(!$shipping_services) {
    return array('no_services' => array('#markup' => t('You must enable at least one shipping service')));
  }
  
  $form['#attached']['js'] = array(
    array(
      'data' => drupal_get_path('module', 'shipping') . '/shipping.js',
      'type' => 'file',
    ),
    array(
      'data' => 'http://maps.googleapis.com/maps/api/js?sensor=false',
      'type' => 'external',
      'weight' => -99,
    ),
  );
  
  $form['id'] = array(
    '#type' => 'value',
    '#value' => isset($warehouse['id']) ? $warehouse['id'] : 0,
  );
  
  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#default_value' => !empty($warehouse['title']) ? $warehouse['title'] : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#description' => t('Name of warehouse'),
    '#weight' => 0,
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textfield',
    '#default_value' => !empty($warehouse['description']) ? $warehouse['description'] : '',
    '#maxlength' => 255,
    '#description' => t('Short description of warehouse, no more than 255 characters.'),
    '#weight' => 10,
  );

  $service = 'default';
  if(!empty($warehouse['service'])) {
    $service = !empty($warehouse['module']) ? $warehouse['service'] . '|' . $warehouse['module'] : $warehouse['service'];
  }

  $form['service'] = array(
    '#type' => 'select',
    '#title' => t('Service'),
    '#options' => $shipping_services,
    '#default_value' => $service['service'],
    '#weight' => 20,
  );

  $suffix = '<div id="google-map-links"><a id="load-map" href="javascript:void(0)">' . t('Show on map') . '</a></div>';
  $suffix .= '<div id="google-map-wrapper" style="display:none;">';
  $suffix .= '<div id="google-map" style="height: 200px; width: 200px;"></div>';
  $suffix .= '</div>';

  $form['address'] = array(
    '#title' => t('Address'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#attributes' => array('id' => 'google-map-address'),
    '#suffix' => $suffix,
    '#weight' => 30,
  );

  $form['address']['address'] = array(
    '#title' => t('Address'),
    '#type' => 'textfield',
    '#description' => t('Street, building, appartment number'),
    '#required' => TRUE,
    '#weight' => -10,
    '#attributes' => array('id' => 'warehouse-address'),
    '#default_value' => !empty($warehouse['address']) ? $warehouse['address'] : '',
    '#autocomplete_path' => 'shipping/address',
  );

  $form['address']['city'] = array(
    '#title' => t('City'),
    '#type' => 'textfield',
    '#description' => t('City name'),
    '#required' => TRUE,
    '#weight' => -5,
    '#attributes' => array('id' => 'warehouse-city'),
    '#default_value' => !empty($warehouse['city']) ? $warehouse['city'] : '',
    '#maxlength' => 255,
    '#autocomplete_path' => 'shipping/city',
  );

  $form['address']['state'] = array(
    '#title' => t('State'),
    '#type' => 'textfield',
    '#description' => t('State/Province name'),
    '#required' => TRUE,
    '#weight' => 0,
    '#attributes' => array('id' => 'warehouse-state'),
    '#default_value' => !empty($warehouse['state']) ? $warehouse['state'] : '',
    '#maxlength' => 255,
    '#autocomplete_path' => 'shipping/state',
  );

  $form['address']['country'] = array(
    '#title' => t('Country'),
    '#type' => 'select',
    '#weight' => 5,
    '#options' => country_get_list(),
    '#default_value' => !empty($warehouse['country']) ? $warehouse['country'] : variable_get('site_default_country', ''),
    '#attributes' => array('id' => 'warehouse-country'),
    '#required' => TRUE,
    '#description' => t('Choose country of the warehouse'),
  );

  $form['map'] = array(
    '#title' => t('Map'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 40,
  );

  $form['map']['lng'] = array(
    '#title' => t('Longtude'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('Longtude of the warehouse location'),
    '#attributes' => array('id' => 'warehouse-lng'),
    '#default_value' => !empty($warehouse['lng']) ? $warehouse['lng'] : '',
    '#element_validate' => array('element_validate_number'),
  );

  $form['map']['lat'] = array(
    '#title' => t('Latitude'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('Latitude of the warehouse location'),
    '#attributes' => array('id' => 'warehouse-lat'),
    '#default_value' => !empty($warehouse['lat']) ? $warehouse['lat'] : '',
    '#element_validate' => array('element_validate_number'),
  );

  $form['data'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#weight' => 50,
  );

  $form['data']['time'] = array(
    '#title' => t('Time'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['data']['time']['week'] = array(
    '#title' => t('Week'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['data']['time']['week']['receive'] = array(
    '#title' => t('Collecting time'),
    '#type' => 'textfield',
    '#size' => 4,
    '#description' => t('Hours when customer can get a package'),
    '#default_value' => !empty($warehouse['data']['time']['week']['receive']) ? $warehouse['data']['time']['week']['receive'] : '',
  );

  $form['data']['time']['week']['send'] = array(
    '#title' => t('Dispatch time'),
    '#type' => 'textfield',
    '#size' => 4,
    '#description' => t('Hours when customer can send a package'),
    '#default_value' => !empty($warehouse['data']['time']['week']['send']) ? $warehouse['data']['time']['week']['send'] : '',
  );

  $form['data']['time']['week']['work'] = array(
    '#title' => t('Opening hours'),
    '#type' => 'textfield',
    '#size' => 4,
    '#description' => t('Hours when customer the branch is open'),
    '#default_value' => !empty($warehouse['data']['time']['week']['work']) ? $warehouse['data']['time']['week']['work'] : '',
  );

  $form['data']['time']['saturday'] = array(
    '#title' => t('Saturday'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['data']['time']['saturday']['receive'] = array(
    '#title' => t('Collecting time'),
    '#type' => 'textfield',
    '#size' => 4,
    '#description' => t('Hours when customer can get a package'),
    '#default_value' => !empty($warehouse['data']['time']['saturday']['receive']) ? $warehouse['data']['time']['saturday']['receive'] : '',
  );

  $form['data']['time']['saturday']['send'] = array(
    '#title' => t('Dispatch time'),
    '#type' => 'textfield',
    '#size' => 4,
    '#description' => t('Hours when customer can send a package'),
    '#default_value' => !empty($warehouse['data']['time']['saturday']['send']) ? $warehouse['data']['time']['saturday']['send'] : '',
  );

  $form['data']['time']['saturday']['work'] = array(
    '#title' => t('Opening hours'),
    '#type' => 'textfield',
    '#size' => 4,
    '#description' => t('Hours when customer the branch is open'),
    '#default_value' => !empty($warehouse['data']['time']['saturday']['work']) ? $warehouse['data']['time']['saturday']['work'] : '',
  );

  $form['data']['phone'] = array(
    '#title' => t('Phone'),
    '#type' => 'textfield',
    '#description' => t('Warehouse phone number'),
    '#default_value' => !empty($warehouse['data']['phone']) ? $warehouse['data']['phone'] : '',
  );

  $form['data']['weight'] = array(
    '#title' => t('Max weight'),
    '#type' => 'textfield',
    '#size' => 4,
    '#description' => t('Maximum weight allowed for this warehouse'),
    '#default_value' => !empty($warehouse['data']['weight']) ? $warehouse['data']['weight'] : '',
  );
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function shipping_warehouse_edit_form_validate($form, &$form_state) {
  form_state_values_clean($form_state);
  if(strpos($form_state['values']['service'], '|') !== FALSE) {
    $exploded = explode('|', $form_state['values']['service'], 2);
    if(!empty($exploded[1])) {
      $form_state['values']['module'] = trim($exploded[1]);
      $form_state['values']['service'] = trim($exploded[0]);
    }
  }
}

function shipping_warehouse_edit_form_submit($form, &$form_state) {
  shipping_warehouse_set($form_state['values']);
  drupal_set_message(t('The configuration options have been saved.'));
}

function shipping_service_delete_form($form, &$form_state, $service) {
  $form_state['service'] = $service;
  
  $form['delete_warehouses'] = array(
    '#title' => t('Delete all warehouses which belong to the service'),
    '#type' => 'checkbox',
    '#default_value' => 0,
  );
  
  return confirm_form($form,
    t('Are you sure you want to delete shipping service %name?', array('%name' => $service['title'])),
    'admin/structure/shipping/services',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );
}

function shipping_service_delete_form_submit($form, &$form_state) {
  shipping_service_delete($form_state['service']['id']);
  if($form_state['values']['delete_warehouses']) {
    db_delete('shipping_warehouses')->condition('service', $form_state['service']['id'])->execute();
  }
  drupal_set_message(t('The configuration options have been saved.'));
}

function shipping_warehouse_delete_form($form, &$form_state, $warehouse) {
  $form_state['warehouse'] = $warehouse;
  return confirm_form($form,
    t('Are you sure you want to delete warehouse %name (ID @id)?', array(
      '%name' => $warehouse->name,
      '@id' => $warehouse->id,
    )),
    'admin/structure/shipping/warehouses',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );
}

function shipping_warehouse_delete_form_submit($form, &$form_state) {
  shipping_warehouse_delete($form_state['warehouse']->id);
  drupal_set_message(t('The configuration options have been saved.'));
}

