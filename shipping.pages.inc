<?php

function shipping_services() {
  return theme('shipping_services');
}

function shipping_warehouses() {
  return theme('shipping_warehouses');
}

function shipping_warehouse_search() {
  return theme('shipping_warehouse_search');
}

function shipping_page() {
  $args = func_get_args();
  if (isset($args[0])) {
    switch($args[0]) {
      case 'markers':
        if (user_access('shipping use google map')) {
          shipping_get_warehouse_json();
          return;
        }
        break;
      case 'search':
        if (user_access('shipping use google map')) {
          return theme('shipping_warehouse_search', array());
        }
        break;
      case 'city':
      case 'address':
      case 'state':
        if (!empty($args[1])) {
          return drupal_json_output(_shipping_autocomplete($args[1], $args[0]));
        }
        break;
      case 'quick-look':
        if(!empty($args[1])) {
          print theme('shipping_warehouse_info', array('warehouse' => $args[1]));
          return NULL;
        }
        break;
    }
  }
  
  return MENU_ACCESS_DENIED;
}

function _shipping_autocomplete($string, $field) {
  $result = db_select('shipping_warehouses', 'w')
    ->fields('w', array($field))
    ->condition('service', '%' . db_like($string) . '%', 'LIKE')
    ->range(0, 10)
    ->execute()
    ->fetchCol();
  return $result ? drupal_map_assoc($result) : array();
}

function shipping_get_warehouse_json() {

  if (empty($_GET['lat']) || empty($_GET['lng'])) {
    return MENU_ACCESS_DENIED;
  }
  
  $output = array();
  if ($results = shipping_get_nearby($_GET['lat'], $_GET['lng'])) {

    foreach ($results as $service_name => $warehouses){
      if (isset($_GET['service']) && $_GET['service'] != $service_name) {
        continue;
      }
      $output['html'] = theme('shipping_search_results', array('warehouses' => $warehouses));
      foreach($warehouses as $warehouse) {
        $output['markers'][] = array('lat' => $warehouse['lat'], 'lng' => $warehouse['lng'], 'id' => $warehouse['id']);
      }
    }
  }
  drupal_json_output($output);
}

function shipping_batch_csv_set($filepath) {
  $batch = array(
    'title' => t('Reading file'),
    'finished' => 'shipping_batch_csv_finished',
    'file' => drupal_get_path('module', 'shipping') . '/shipping.pages.inc',
    'operations' => array(
      array(
        'shipping_batch_csv_process', array($filepath),
      ),
    ),
  );
  batch_set($batch);
}

function shipping_batch_csv_process($filepath, &$context) {
  
  $batch_limit = 10;
  $context['finished'] = 0;

  $file_handle = fopen($filepath, 'r');
  if (isset($context['sandbox']['file_pointer_position'])) {
    fseek($file_handle, $context['sandbox']['file_pointer_position']);
  }
  else {
    $context['sandbox']['file_pointer_position'] = 0;
  }
  
  $header = fgetcsv($file_handle, 0, ';');
  for ($i = 0; $i < $batch_limit; $i++) {
    $csv_line = fgetcsv($file_handle, 0, ';');
    if (is_array($csv_line)) {
      if ($result = shipping_batch_csv_import($csv_line)) {
        $context['results'][] = $result;
      }
    }

    $context['sandbox']['file_pointer_position'] = ftell($file_handle);
    if (feof($file_handle)) {
      $context['finished'] = 1;
      break;
    }
  }
}

function shipping_batch_csv_finished($success, $results, $operations) {
  if ($success) {
    $message = t('Processed @count rows', array('@count' => count($results)));
    drupal_set_message($message);
  }
  else {
    $message = t('An error occurred while processing CSV file');
  }
}

/**
 * Expected format: ID title description service country state city address lat lng phone max_weight time_week time_saturday
*/
function shipping_batch_csv_import($csv_line) {

  array_walk($csv_line, function(&$value) {
    $value = truncate_utf8(trim($value), 255);
  });

  if(count($csv_line) < 14) {
    return;
  }

  if ($csv_line[1] == "" || $csv_line[3] == "" || $csv_line[6] == "" || $csv_line[7] == "") {
    return;
  }

  if ($csv_line[0] == "") {
    $csv_line[0] = NULL;
  }

  if ($csv_line[4] == "") {
    $csv_line[4] = variable_get('site_default_country', '');
  }
  else {
    $countries = country_get_list();
    if (!isset($countries[$csv_line[4]])) {
      $csv_line[4] = variable_get('site_default_country', '');
    }
  }
  
  if($csv_line[8] == "" || $csv_line[9] == "") {
    if(!variable_get('shipping_upload_geocode', 0)) {
      return;
    }
    $address = $csv_line[7] . ', ' . $csv_line[6] . ', ' . $csv_line[5] . ', ' . $csv_line[4];
    $geocoded = shipping_geocode_address($address);
    if(!empty($geocoded['lat']) && !empty($geocoded['lng'])) {
      $csv_line[8] = trim($geocoded['lat']);
      $csv_line[9] = trim($geocoded['lng']);
    }
  }
  
  $time_week = array();
  if($csv_line[12] != "") {
    $exploded = explode('|', $csv_line[12], 3);
    $time_week['work'] = trim($exploded[0]);
    $time_week['receive'] = !empty($exploded[1]) ? trim($exploded[1]) : $time_week['work'];
    $time_week['send'] = !empty($exploded[2]) ? trim($exploded[2]) : $time_week['work'];
  }

  $time_saturday = array();
  if($csv_line[13] != "") {
    $exploded = explode('|', $csv_line[13], 3);
    $time_saturday['work'] = trim($exploded[0]);
    $time_saturday['receive'] = !empty($exploded[1]) ? trim($exploded[1]) : $time_saturday['work'];
    $time_saturday['send'] = !empty($exploded[2]) ? trim($exploded[2]) : $time_saturday['work'];
  }

  $module = '';
  if(strpos($csv_line[3], '|') !== FALSE) {
    $exploded = explode('|', $csv_line[3], 2);
    if(!empty($exploded[1])) {
      $module = $exploded[1];
    }
  }
  
  $warehouse = array(
    'id' => $csv_line[0],
    'title' => $csv_line[1],
    'description' => $csv_line[2],
    'service' => $csv_line[3],
    'module' => $module,
    'country' => $csv_line[4],
    'state' => $csv_line[5],
    'city' => $csv_line[6],
    'address' => $csv_line[7],
    'lat' => $csv_line[8],
    'lng' => $csv_line[9],
    'data' => array(
      'time' => array(
        'week' => $time_week,
        'saturday' => $time_saturday,
      ),
      'phone' => $csv_line[10],
      'weight' => $csv_line[11],
    ),
  );

  shipping_warehouse_set($warehouse);
  return TRUE;
}

